from flask import Flask, request
import RPi.GPIO as GPIO
import time

app = Flask(__name__)

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(14, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)

def setServoAngle(serPin, angle):
   tilt = GPIO.PWM(serPin, 50)
   tilt.start(0)
   DutyCycle = angle/18 + 2
   tilt.ChangeDutyCycle(DutyCycle)
   time.sleep(0.05)
   tilt.stop()

@app.route('/')
def root():
    return "Hello!"

@app.route('/setAngle')
def setAngle():
    s1_angle = int(request.args.get("s1"))
    setServoAngle(14, s1_angle)
    s2_angle = int(request.args.get("s2"))
    setServoAngle(15, s2_angle)
    return 'S1: {}, S2: {}'.format(s1_angle, s2_angle)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
# 树莓派与PC-双轴舵机人脸追踪

## 前言
本来想用UDP通信，不知为何在PC上测试成功，在树莓派socketsendall就报错。
可以不用树莓派，用单片机来驱动舵机，用USB的摄像头更方便
摄像头已经装好了就没拆了，所以就用树莓派输出HDMI到PC作为视频输入

## 接线
树莓派-舵机
pin14 - 纵轴舵机（负责左右方向的舵机）
pin15 - 横轴舵机（负责上下方向的舵机）

## Python模块安装
### 树莓派
    pip3 install flask

### PC
    pip install opencv-python
    pip install mediapipe

## 运行
树莓派与PC需在同一个局域网中

### 树莓派
运行树莓派文件夹下的demo.py
在终端再运行命令
    $raspistill -t 3600000 #树莓派显示摄像头画面1小时

### PC
运行PC文件夹下的demo.py（注意将IP地址改为树莓派的IP地址）

## 效果图
![输入图片说明](%E5%9B%BE%E7%89%87/Screenshot_20220419_225328_com.huawei.himovie.jpg)
![输入图片说明](%E5%9B%BE%E7%89%87/IMG_20220419_225302.jpg)

## 视频
[点击打开视频页面](https://gitee.com/chenxi2000/face-tracking-ptz/tree/master/%E8%A7%86%E9%A2%91)